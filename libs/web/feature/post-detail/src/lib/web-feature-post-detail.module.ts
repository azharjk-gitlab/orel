import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';

import { WebUiPostCardModule } from '@orel/web/ui/post-card';
import { WebFeatureCommentListModule } from '@orel/web/feature/comment-list';
import { WebSharedDirectiveModule } from '@orel/web/shared/directive';
import { PostDetailComponent } from './post-detail/post-detail.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    WebUiPostCardModule,
    WebFeatureCommentListModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    WebSharedDirectiveModule,
    MatDialogModule,
  ],
  declarations: [PostDetailComponent],
})
export class WebFeaturePostDetailModule {}
