import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import {
  InvalidIdFormatError,
  PostService,
  ResourceNotFoundError
} from '@orel/web/data-access/post';
import { CommentService } from '@orel/web/data-access/comment';
import { Post } from '@orel/web/data-access/post';
import { PostEditDialogComponent } from '@orel/web/feature/post-edit-dialog';

@Component({
  selector: 'orel-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss'],
})
export class PostDetailComponent implements OnInit {
  commentControl = new FormControl('', [Validators.required]);

  post: Post | undefined;

  constructor(
    private postService: PostService,
    private commentService: CommentService,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.postService.findUnique(+params['id']).subscribe({
        next: (data) => (this.post = data),
        error: (err) => {
          if (err instanceof ResourceNotFoundError) alert(err.message);
          if (err instanceof InvalidIdFormatError) alert(err.message);
        },
      });
    });
  }

  onCommentSubmit() {
    if (this.commentControl.invalid) {
      alert('Comment should not be empty');
      return;
    }

    if (!this.post) {
      alert('Wierd post is undefined while commenting');
      return;
    }

    this.commentService
      .create({
        message: this.commentControl.value as string,
        postId: this.post.id,
      })
      .subscribe();
  }

  onEditPostClick() {
    this.dialog.open(PostEditDialogComponent, {
      width: '500px',
      data: {
        content: this.post?.content,
        id: this.post?.id,
      },
    });
  }

  onDeletePostClick() {
    if (!this.post?.id) return;
    this.postService.delete(this.post?.id).subscribe();
  }
}
