import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

import { PostService } from '@orel/web/data-access/post';
import { Post } from '@orel/web/data-access/post';

@Component({
  selector: 'orel-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit {
  posts$!: Observable<Post[]> | undefined;

  createPostForm = new FormGroup({
    content: new FormControl('', [Validators.required]),
  });

  constructor(private postService: PostService) {}

  ngOnInit(): void {
    this.posts$ = this.postService.findMany();
  }

  onCreatePostSubmit() {
    if (this.createPostForm.invalid) return alert('FORM IS INVALID');

    this.postService.create(this.createPostForm.value as Post).subscribe();
  }
}
