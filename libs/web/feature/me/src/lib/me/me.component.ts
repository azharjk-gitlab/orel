import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { MeService, Me } from '@orel/web/data-access/me';

@Component({
  selector: 'orel-me',
  templateUrl: './me.component.html',
  styleUrls: ['./me.component.scss'],
})
export class MeComponent implements OnInit {
  me$: Observable<Me> | undefined;

  constructor(private meService: MeService) {}

  ngOnInit(): void {
    this.me$ = this.meService.getMe();
  }
}
