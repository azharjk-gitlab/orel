import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService, AuthToken, BackendValidationError, InvalidCredentialsError } from '@orel/web/data-access/auth';
import { ACCESS_TOKEN_NAME } from '@orel/web/shared/constant';

@Component({
  selector: 'orel-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
})
export class SigninComponent {
  // NOTE: The validation between the backend and the frontend is different
  //       especially on the email field. On the frontend 'a@a' is a valid
  //       email but on the backend is invalid so be careful. And for now
  //       its needs to catch a 400 (Bad Request) error from the backend
  //       and proceed that to the end user.
  signinForm = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
    password: new FormControl('', [
      Validators.required,
    ]),
  });

  @ViewChild('passwordRef') passwordRef: ElementRef<HTMLInputElement> | undefined;

  isShowPassword = false;

  constructor(private authService: AuthService, private router: Router) {}

  onShowPasswordChange() {
    if (!this.passwordRef) return;
    this.passwordRef.nativeElement.type = this.isShowPassword ? 'text' : 'password';
  }

  onSigninSubmit() {
    if (this.signinForm.invalid) {
      alert('[ERROR]: Invalid sign in form input');
      return;
    }

    this.authService.signIn(this.signinForm.value as any)
      .subscribe({
        next: (data: AuthToken) => localStorage.setItem(ACCESS_TOKEN_NAME, data.access_token),
        error: (err) => {
          if (err instanceof BackendValidationError) alert(err.message);
          if (err instanceof InvalidCredentialsError) alert(err.message);
        },
        complete: () => this.router.navigateByUrl('/'),
      });
  }
}
