import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { WebUiAuthFormModule } from '@orel/web/ui/auth-form';
import { WebDataAccessAuthModule } from '@orel/web/data-access/auth';
import { SigninComponent } from './signin/signin.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    WebDataAccessAuthModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    WebUiAuthFormModule,
  ],
  declarations: [SigninComponent],
})
export class WebFeatureSigninModule {}
