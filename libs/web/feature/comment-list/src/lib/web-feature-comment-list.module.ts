import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';

import { CommentListComponent } from './comment-list/comment-list.component';

@NgModule({
  imports: [CommonModule, MatCardModule],
  declarations: [CommentListComponent],
  exports: [CommentListComponent],
})
export class WebFeatureCommentListModule {}
