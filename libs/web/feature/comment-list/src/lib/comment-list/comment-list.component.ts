import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { CommentService, Comment } from '@orel/web/data-access/comment';

@Component({
  selector: 'orel-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.scss'],
})
export class CommentListComponent implements OnInit {
  @Input() postId: number | undefined;

  comments$: Observable<Comment[]> | undefined;

  constructor(private commentService: CommentService) {}

  ngOnInit(): void {
    if (!this.postId) return;
    this.comments$ = this.commentService.findManyCommentOnPost(this.postId);
  }
}
