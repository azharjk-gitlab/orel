import { Component, ElementRef, QueryList, ViewChildren } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService, AuthToken, BackendValidationError, EmailIsExistsError } from '@orel/web/data-access/auth';
import { ACCESS_TOKEN_NAME } from '@orel/web/shared/constant';

@Component({
  selector: 'orel-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent {
  signupForm = new FormGroup(
    {
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
      ]),
      repeatPassword: new FormControl('', [Validators.required]),
    },
    { validators: this.matchRepeatedPassword }
  );

  @ViewChildren('passwordRef') passwordRef: QueryList<ElementRef<HTMLInputElement>> | undefined;

  isShowPassword = false;

  constructor(private authService: AuthService, private router: Router) { }

  matchRepeatedPassword(group: AbstractControl): ValidationErrors | null {
    const password = group.get('password')?.value;
    const repeatPassword = group.get('repeatPassword')?.value;

    return password === repeatPassword ? null : { isMatch: false };
  }

  onShowPasswordChange() {
    if (!this.passwordRef) return;
    this.passwordRef.forEach((ref) => {
      ref.nativeElement.type = this.isShowPassword ? 'text' : 'password';
    });
  }

  onSignupSubmit() {
    if (this.signupForm.invalid) {
      alert('[ERROR]: Invalid sign in form input');
      return;
    }

    this.authService.signUp(this.signupForm.value as any)
      .subscribe({
        next: (data: AuthToken) => localStorage.setItem(ACCESS_TOKEN_NAME, data.access_token),
        error: (err) => {
          if (err instanceof BackendValidationError) alert(err.message);
          if (err instanceof EmailIsExistsError) alert(err.message);
        },
        complete: () => this.router.navigateByUrl('/'),
      });
  }
}
