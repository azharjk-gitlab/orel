import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Socket } from 'ngx-socket-io';

import {
  GlobalChatService,
  GlobalChatMessage
} from '@orel/web/data-access/global-chat';

@Component({
  selector: 'orel-global-chat',
  templateUrl: './global-chat.component.html',
  styleUrls: ['./global-chat.component.scss'],
})
export class GlobalChatComponent implements OnInit {
  messages$: Observable<GlobalChatMessage[]> | undefined;

  messageControl = new FormControl('', [Validators.required]);

  constructor(private globalChatService: GlobalChatService, private socket: Socket) {}

  ngOnInit(): void {
    this.messages$ = this.globalChatService.getMessages();

    this.globalChatService.fromAddMessageEvent().subscribe(() => {
      this.messages$ = this.globalChatService.getMessages();
    });
  }

  onMessageSubmit() {
    if (this.messageControl.invalid) {
      alert('Message is required to be sent');
      return;
    }

    this.globalChatService
      .addMessage(this.messageControl.value as string)
      .subscribe();
  }
}
