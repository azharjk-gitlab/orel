import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PostService } from '@orel/web/data-access/post';

@Component({
  selector: 'orel-post-edit-dialog',
  templateUrl: './post-edit-dialog.component.html',
  styleUrls: ['./post-edit-dialog.component.scss'],
})
export class PostEditDialogComponent {
  // FIXME: This should use form group or form control
  content: string = this.data.content;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: { content: string; id: number },
    private postService: PostService
  ) {}

  onPostEditSaveClick() {
    this.postService.update(this.data.id, this.content).subscribe();
  }
}
