import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '@orel/web/data-access/user';

import { UserService } from '@orel/web/data-access/user';
import { MeService } from '@orel/web/data-access/me';

@Component({
  selector: 'orel-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {
  users$: Observable<User[]> | undefined;

  constructor(private userService: UserService, private meService: MeService) {}

  ngOnInit(): void {
    this.users$ = this.userService.getUsers();
  }

  onFollowClick(targetFollowId: number) {
    this.meService.follow(targetFollowId).subscribe();
  }

  onUnfollowClick(targetUnfollowId: number) {
    this.meService.unfollow(targetUnfollowId).subscribe();
  }
}
