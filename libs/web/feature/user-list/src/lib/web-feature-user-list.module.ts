import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebUiUserCardModule } from '@orel/web/ui/user-card';
import { MatButtonModule } from '@angular/material/button';

import { UserListComponent } from './user-list/user-list.component';

@NgModule({
  imports: [CommonModule, WebUiUserCardModule, MatButtonModule],
  declarations: [UserListComponent],
})
export class WebFeatureUserListModule {}
