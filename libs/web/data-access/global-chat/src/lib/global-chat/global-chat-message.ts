import { GlobalChatMessage as PrismaGlobalChatMessage } from '@prisma/client';

export interface GlobalChatMessage extends PrismaGlobalChatMessage {
  sender: {
    email: string;
  };
  isSender: boolean;
}
