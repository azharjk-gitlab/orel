import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, tap, throwError } from 'rxjs';
import { Socket } from 'ngx-socket-io';

import { API_GLOBAL_CHAT_URL } from '@orel/web/shared/constant';
import { GlobalChatMessage } from './global-chat-message';
import { WS_GLOBAL_CHAT_ADD_MESSAGE } from '@orel/shared/ws';

@Injectable({
  providedIn: 'root',
})
export class GlobalChatService {
  constructor(private http: HttpClient, private socket: Socket) {}

  handleError() {
    return throwError(() => new Error('GlobalChatService: Something happened'));
  }

  getMessages() {
    return this.http
      .get<GlobalChatMessage[]>(API_GLOBAL_CHAT_URL)
      .pipe(catchError(this.handleError));
  }

  addMessage(message: string) {
    return this.http
      .post<GlobalChatMessage>(API_GLOBAL_CHAT_URL, { message })
      .pipe(
        catchError(this.handleError),
        tap(() => this.socket.emit(WS_GLOBAL_CHAT_ADD_MESSAGE))
      );
  }

  fromAddMessageEvent() {
    return this.socket.fromEvent(WS_GLOBAL_CHAT_ADD_MESSAGE);
  }
}
