import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, throwError } from 'rxjs';

import { API_POSTS_URL } from '@orel/web/shared/constant';
import { Comment } from './comment';

interface CommentCreate {
  message: string;
  postId: number;
}

@Injectable({
  providedIn: 'root',
})
export class CommentService {
  constructor(private http: HttpClient) {}

  handleError() {
    return throwError(
      () =>
        new Error(
          'CommentService: Something bad happened'
        )
    );
  }

  findManyCommentOnPost(postId: number) {
    return this.http
      .get<Comment[]>(`${API_POSTS_URL}/${postId}/comments`)
      .pipe(catchError(this.handleError));
  }

  create(data: CommentCreate) {
    return this.http
      .post<Comment>(`${API_POSTS_URL}/${data.postId}/comments`, {
        message: data.message,
      })
      .pipe(catchError(this.handleError));
  }
}
