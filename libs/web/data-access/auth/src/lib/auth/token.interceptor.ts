import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ACCESS_TOKEN_NAME } from '@orel/web/shared/constant';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const accessToken = localStorage.getItem(ACCESS_TOKEN_NAME);

    if (!accessToken) next.handle(req);

    return next.handle(
      req.clone({ setHeaders: { Authorization: `Bearer ${accessToken}` } })
    );
  }
}
