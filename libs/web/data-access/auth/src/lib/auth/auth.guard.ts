import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  UrlTree
} from '@angular/router';
import { map, Observable } from 'rxjs';

import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(): Observable<boolean | UrlTree> {
    return this.authService
      .isAuthenticated()
      .pipe(
        map((isAuthenticated) =>
          isAuthenticated
            ? isAuthenticated
            : this.router.createUrlTree(['/signin'])
        )
      );
  }
}
