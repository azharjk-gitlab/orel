import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, of, throwError, map } from 'rxjs';

import {
  API_SIGNIN_URL,
  API_SIGNUP_URL,
  API_ME_URL
} from '@orel/web/shared/constant';
import {
  BackendValidationError,
  EmailIsExistsError,
  InvalidCredentialsError
} from './auth.error';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  handleSignError(err: HttpErrorResponse) {
    if (err.status === 400) {
      return throwError(
        () =>
          new BackendValidationError('Server responded with 400 bad request')
      );
    }
    // FIXME: Log the error to logger
    return throwError(() => new Error('Something bad happened'));
  }

  // FIXME: Change parameter name collision
  signIn(data: any) {
    return this.http.post(API_SIGNIN_URL, data).pipe(
      catchError(this.handleSignError),
      // FIXME: This should fix at the backend first because
      //        it have different data shape between success state
      //        and the error state.
      map((data: any) => {
        if (data.type === 'INVALID_CREDENTIALS')
          throw new InvalidCredentialsError('Invalid credentials');
        return data;
      })
    );
  }

  // FIXME: Change parameter name collision
  signUp(data: any) {
    return this.http.post(API_SIGNUP_URL, data).pipe(
      catchError(this.handleSignError),
      map((data: any) => {
        if (data.type === 'EMAIL_IS_EXISTS')
          throw new EmailIsExistsError('Sorry email is exists');
        return data;
      })
    );
  }

  isAuthenticated() {
    return this.http.get<boolean>(API_ME_URL).pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.status === 401) return of(false);
        // FIXME: Log the error to logger
        return throwError(() => new Error('Something bad happened'));
      }),
      map((isAuthenticated) => (!isAuthenticated ? false : true))
    );
  }
}
