export class BackendValidationError extends Error {
  override name = 'BackendValidationError';

  constructor(message: string) {
    super(message);
  }
}

export class InvalidCredentialsError extends Error {
  override name = 'InvalidCredentialsError';

  constructor(message: string) {
    super(message);
  }
}

export class EmailIsExistsError extends Error {
  override name = 'EmailIsExistsError';

  constructor(message: string) {
    super(message);
  }
}
