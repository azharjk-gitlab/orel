import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, throwError } from 'rxjs';

import { API_POSTS_URL } from '@orel/web/shared/constant';
import { InvalidIdFormatError, ResourceNotFoundError } from './post.error';
import { Post } from './post';

@Injectable({
  providedIn: 'root',
})
export class PostService {
  constructor(private http: HttpClient) {}

  handleError() {
    // FIXME: Log the error to logger
    return throwError(() => new Error('Something bad happened'));
  }

  findMany() {
    return this.http
      .get<Post[]>(API_POSTS_URL)
      .pipe(catchError(this.handleError));
  }

  findUnique(id: number) {
    return this.http.get<Post>(`${API_POSTS_URL}/${id}`).pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.status === 404)
          return throwError(() => new ResourceNotFoundError());
        if (err.status === 400)
          return throwError(() => new InvalidIdFormatError());
        return throwError(
          () => new Error('PostService#findUnique: Something bad happened')
        );
      })
    );
  }

  delete(id: number) {
    return this.http
      .delete(`${API_POSTS_URL}/${id}`)
      .pipe(catchError(this.handleError));
  }

  update(id: number, content: string) {
    return this.http
      .put<Post>(`${API_POSTS_URL}/${id}`, { content })
      .pipe(catchError(this.handleError));
  }

  create(post: Post) {
    return this.http
      .post<Post[]>(API_POSTS_URL, post)
      .pipe(catchError(this.handleError));
  }
}
