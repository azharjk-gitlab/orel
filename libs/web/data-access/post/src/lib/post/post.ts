import { Post as PrismaPost } from '@prisma/client';

export interface Post extends PrismaPost {
  creator: {
    email: string;
  }
}
