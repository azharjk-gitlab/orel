export class ResourceNotFoundError extends Error {
  override name = 'ResourceNotFoundError';

  constructor() {
    super('Resource not found');
  }
}

export class InvalidIdFormatError extends Error {
  override name = 'InvalidIdFormatError';

  constructor() {
    super('Invalid id format');
  }
}
