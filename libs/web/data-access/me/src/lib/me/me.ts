export interface Me {
  id: number;
  email: string;
  createdAt: string;
}
