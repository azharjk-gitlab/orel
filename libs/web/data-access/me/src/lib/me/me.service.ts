import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, throwError } from 'rxjs';

import {
  API_ME_FOLLOW_URL,
  API_ME_UNFOLLOW_URL,
  API_ME_URL
} from '@orel/web/shared/constant';
import { Me } from './me';

@Injectable({
  providedIn: 'root',
})
export class MeService {
  constructor(private http: HttpClient) {}

  getMe() {
    return this.http.get<Me>(API_ME_URL).pipe(
      catchError(() => {
        // FIXME: Log the error to logger
        return throwError(() => new Error('Something bad happend'));
      })
    );
  }

  follow(targetFollowId: number) {
    return this.http.post(API_ME_FOLLOW_URL, { targetFollowId });
  }

  unfollow(targetUnfollowId: number) {
    return this.http.post(API_ME_UNFOLLOW_URL, { targetUnfollowId });
  }
}
