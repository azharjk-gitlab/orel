import { FollowingFollowers, User as PrismaUser } from '@prisma/client';

export interface User extends PrismaUser {
  following: FollowingFollowers[];
  followers: FollowingFollowers[];
  isAlreadyFollowed: boolean;
}
