export const API_BASE_URL = 'http://localhost:3333';

export const API_SIGNIN_URL = `${API_BASE_URL}/api/auth/signin`;
export const API_SIGNUP_URL = `${API_BASE_URL}/api/auth/signup`;

export const API_ME_URL = `${API_BASE_URL}/api/me`;
export const API_ME_FOLLOW_URL = `${API_ME_URL}/follow`;
export const API_ME_UNFOLLOW_URL = `${API_ME_URL}/unfollow`;

export const API_POSTS_URL = `${API_BASE_URL}/api/posts`;

export const API_USERS_URL = `${API_BASE_URL}/api/users`;

export const API_GLOBAL_CHAT_URL = `${API_BASE_URL}/api/global-chat`;

export const ACCESS_TOKEN_NAME = 'access_token';
