import { webSharedConstant } from './web-shared-constant';

describe('webSharedConstant', () => {
  it('should work', () => {
    expect(webSharedConstant()).toEqual('web-shared-constant');
  });
});
