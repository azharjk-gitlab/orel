import {
  Directive,
  Input,
  OnInit,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';

import { MeService } from '@orel/web/data-access/me';

@Directive({
  selector: '[orelIsPostCreator]',
})
export class IsPostCreatorDirective implements OnInit {
  // creatorId
  @Input() orelIsPostCreator: number | undefined;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainerRef: ViewContainerRef,
    private meService: MeService
  ) {}

  ngOnInit(): void {
    this.meService.getMe().subscribe((data) => {
      if (data.id === this.orelIsPostCreator) {
        this.viewContainerRef.createEmbeddedView(this.templateRef);
      }
    });
  }
}
