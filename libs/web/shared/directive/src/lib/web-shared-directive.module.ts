import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WebDataAccessMeModule } from '@orel/web/data-access/me';
import { IsPostCreatorDirective } from './is-post-creator.directive';

@NgModule({
  imports: [CommonModule, WebDataAccessMeModule],
  declarations: [IsPostCreatorDirective],
  exports: [IsPostCreatorDirective],
})
export class WebSharedDirectiveModule {}
