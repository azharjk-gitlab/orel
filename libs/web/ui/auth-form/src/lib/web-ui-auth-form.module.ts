import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WebUiFooterModule } from '@orel/web/ui/footer';
import { AuthFormComponent } from './auth-form/auth-form.component';

@NgModule({
  imports: [CommonModule, WebUiFooterModule],
  declarations: [AuthFormComponent],
  exports: [AuthFormComponent],
})
export class WebUiAuthFormModule {}
