import { Component, Input } from '@angular/core';
import { User } from '@prisma/client';

@Component({
  selector: 'orel-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss'],
})
export class UserCardComponent {
  @Input() user: User | undefined;
}
