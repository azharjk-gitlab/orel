import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';

import { UserCardComponent } from './user-card/user-card.component';

@NgModule({
  imports: [CommonModule, MatCardModule],
  declarations: [UserCardComponent],
  exports: [UserCardComponent],
})
export class WebUiUserCardModule {}
