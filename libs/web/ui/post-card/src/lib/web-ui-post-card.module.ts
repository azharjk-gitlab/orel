import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';

import { PostCardComponent } from './post-card/post-card.component';

@NgModule({
  imports: [CommonModule, MatCardModule],
  declarations: [PostCardComponent],
  exports: [PostCardComponent],
})
export class WebUiPostCardModule {}
