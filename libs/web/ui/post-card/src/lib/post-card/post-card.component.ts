import { Component, Input } from '@angular/core';

import { Post } from '@orel/web/data-access/post';

@Component({
  selector: 'orel-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.scss'],
})
export class PostCardComponent {
  @Input()
    post: Post | undefined;
}
