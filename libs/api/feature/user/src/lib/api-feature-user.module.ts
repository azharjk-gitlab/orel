import { Module } from '@nestjs/common';

import { ApiDataAccessUserModule } from '@orel/api/data-access/user';
import { UserController } from './user/user.controller';

@Module({
  controllers: [UserController],
  providers: [],
  exports: [],
  imports: [ApiDataAccessUserModule],
})
export class ApiFeatureUserModule {}
