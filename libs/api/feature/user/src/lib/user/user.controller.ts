import { Controller, Get, UseGuards } from '@nestjs/common';
import { User } from '@prisma/client';

import { JwtAuthGuard, User as AuthUser } from '@orel/api/data-access/auth';
import { UserService } from '@orel/api/data-access/user';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  findMany(@AuthUser() user: User) {
    return this.userService.findMany(user.id);
  }
}
