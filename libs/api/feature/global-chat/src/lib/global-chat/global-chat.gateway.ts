import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer
} from '@nestjs/websockets';
import { Server } from 'socket.io';

import { WS_GLOBAL_CHAT_ADD_MESSAGE } from '@orel/shared/ws';

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class GlobalChatGateway {
  @WebSocketServer() server: Server;

  @SubscribeMessage(WS_GLOBAL_CHAT_ADD_MESSAGE)
  handleAddMessage() {
    this.server.sockets.emit(WS_GLOBAL_CHAT_ADD_MESSAGE);
  }
}
