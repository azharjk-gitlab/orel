import { Test, TestingModule } from '@nestjs/testing';
import { GlobalChatController } from './global-chat.controller';

describe('GlobalChatController', () => {
  let controller: GlobalChatController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GlobalChatController],
    }).compile();

    controller = module.get<GlobalChatController>(GlobalChatController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
