import { IsNotEmpty } from 'class-validator';

export class GlobalChatAddMessageBody {
  @IsNotEmpty() message: string;
}
