import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { User } from '@prisma/client';

import { JwtAuthGuard, User as AuthUser } from '@orel/api/data-access/auth';
import { GlobalChatService } from '@orel/api/data-access/global-chat';
import { GlobalChatAddMessageBody } from './global-chat';

@Controller('global-chat')
export class GlobalChatController {
  constructor(private globalChatService: GlobalChatService) { }

  @UseGuards(JwtAuthGuard)
  @Get()
  getMessages(@AuthUser() user: User) {
    return this.globalChatService.getMessages(user.id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  addMessage(@Body() body: GlobalChatAddMessageBody, @AuthUser() sender: User) {
    return this.globalChatService.addMessage({
      message: body.message,
      senderId: sender.id,
    });
  }
}
