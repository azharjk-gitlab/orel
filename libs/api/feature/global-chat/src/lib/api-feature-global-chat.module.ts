import { Module } from '@nestjs/common';

import { ApiDataAccessGlobalChatModule } from '@orel/api/data-access/global-chat';
import { GlobalChatController } from './global-chat/global-chat.controller';
import { GlobalChatGateway } from './global-chat/global-chat.gateway';

@Module({
  controllers: [GlobalChatController],
  providers: [GlobalChatGateway],
  exports: [],
  imports: [ApiDataAccessGlobalChatModule],
})
export class ApiFeatureGlobalChatModule {}
