# api-feature-global-chat

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test api-feature-global-chat` to execute the unit tests via [Jest](https://jestjs.io).
