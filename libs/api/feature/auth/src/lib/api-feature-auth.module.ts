import { Module } from '@nestjs/common';

import { ApiDataAccessAuthModule } from '@orel/api/data-access/auth';
import { AuthController } from './auth/auth.controller';

@Module({
  controllers: [AuthController],
  imports: [ApiDataAccessAuthModule],
})
export class ApiFeatureAuthModule {}
