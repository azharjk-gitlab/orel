# api-feature-me

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test api-feature-me` to execute the unit tests via [Jest](https://jestjs.io).
