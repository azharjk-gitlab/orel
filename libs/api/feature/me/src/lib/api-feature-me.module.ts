import { Module } from '@nestjs/common';

import { ApiDataAccessPrismaModule } from '@orel/api/data-access/prisma';
import { MeController } from './me/me.controller';
import { MeService } from './me/me.service';

@Module({
  controllers: [MeController],
  providers: [MeService],
  exports: [],
  imports: [ApiDataAccessPrismaModule],
})
export class ApiFeatureMeModule {}
