import { IsNotEmpty, IsNumber } from 'class-validator';

export class FollowUserDto {
  @IsNotEmpty() @IsNumber() targetFollowId: number;
}

export class UnfollowUserDto {
  @IsNotEmpty() @IsNumber() targetUnfollowId: number;
}
