import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { User } from '@prisma/client';

import { JwtAuthGuard, User as AuthUser } from '@orel/api/data-access/auth';
import { MeService } from './me.service';
import { FollowUserDto, UnfollowUserDto } from './dto';

@Controller('me')
export class MeController {
  constructor(private meService: MeService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  getMe(@AuthUser() user: User) {
    return this.meService.getMe(user);
  }

  @UseGuards(JwtAuthGuard)
  @Post('follow')
  follow(@Body() followUserDto: FollowUserDto, @AuthUser() user: User) {
    return this.meService.follow(user.id, followUserDto.targetFollowId);
  }

  @UseGuards(JwtAuthGuard)
  @Post('unfollow')
  unfollow(@Body() unfollowUserDto: UnfollowUserDto, @AuthUser() user: User) {
    return this.meService.unfollow(user.id, unfollowUserDto.targetUnfollowId);
  }
}
