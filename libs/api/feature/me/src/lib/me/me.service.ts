import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common';
import { User } from '@prisma/client';

import { PrismaService } from '@orel/api/data-access/prisma';

@Injectable()
export class MeService {
  constructor(private prisma: PrismaService) {}

  getMe(user: User) {
    const { password, ...rest } = user;
    return rest;
  }

  async follow(userId: number, targetFollowId: number) {
    // Check if the user follow them self
    if (userId === targetFollowId) throw new BadRequestException();

    const target = await this.prisma.user.findUnique({
      where: {
        id: targetFollowId,
      },
    });

    if (!target) throw new NotFoundException();

    const isAlreadyFollowed = await this.prisma.followingFollowers.findFirst({
      where: {
        AND: [{ followerId: userId }, { followingId: targetFollowId }],
      },
    });

    if (isAlreadyFollowed) throw new BadRequestException();

    return this.prisma.followingFollowers.create({
      data: {
        followerId: userId,
        followingId: targetFollowId,
      },
    });
  }

  async unfollow(userId: number, targetUnfollowId: number) {
    const isAlreadyFollowedObj = await this.prisma.followingFollowers.findFirst(
      {
        where: {
          AND: [{ followerId: userId }, { followingId: targetUnfollowId }],
        },
      }
    );

    if (!isAlreadyFollowedObj) throw new BadRequestException();

    await this.prisma.followingFollowers.delete({
      where: {
        id: isAlreadyFollowedObj.id,
      },
    });

    return {
      status: 'OK',
    };
  }
}
