import { Module } from '@nestjs/common';

import { ApiDataAccessPostModule } from '@orel/api/data-access/post';
import { PostController } from './post/post.controller';

@Module({
  imports: [ApiDataAccessPostModule],
  controllers: [PostController],
  exports: [],
})
export class ApiFeaturePostModule {}
