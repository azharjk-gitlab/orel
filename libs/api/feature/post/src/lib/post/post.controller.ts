import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards
} from '@nestjs/common';
import { User } from '@prisma/client';

import { JwtAuthGuard, User as AuthUser } from '@orel/api/data-access/auth';
import { PostService } from '@orel/api/data-access/post';
import { FindUniquePostParam } from './dto/find-unique.dto';
import { PostCreateDto } from './dto/post-create.dto';
import { PostUpdateDto } from './dto/post-update.dto';
import { AddCommentOnPostBody } from './post.body';
import { PostCreatorGuard } from './post-creator.guard';

@Controller('posts')
export class PostController {
  constructor(private postService: PostService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  findMany() {
    return this.postService.findMany();
  }

  @Get(':id')
  findUnique(@Param() findUniquePostParam: FindUniquePostParam) {
    return this.postService.findUniqueOrThrowById(+findUniquePostParam.id);
  }

  @UseGuards(JwtAuthGuard, PostCreatorGuard)
  @Put(':id')
  update(
    @Body() postUpdateDto: PostUpdateDto,
    @Param() findUniquePostParam: FindUniquePostParam
  ) {
    return this.postService.update(+findUniquePostParam.id, {
      ...postUpdateDto,
    });
  }

  @UseGuards(JwtAuthGuard, PostCreatorGuard)
  @Delete(':id')
  delete(@Param() findUniquePostParam: FindUniquePostParam) {
    return this.postService.delete(+findUniquePostParam.id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() postCreateDto: PostCreateDto, @AuthUser() user: User) {
    return this.postService.create({ ...postCreateDto, creatorId: user.id });
  }

  @Get(':id/comments')
  findManyComment(@Param() findUniquePostParam: FindUniquePostParam) {
    return this.postService.findManyComment(+findUniquePostParam.id);
  }

  @UseGuards(JwtAuthGuard)
  @Post(':id/comments')
  addComment(
    @Body() addCommentOnPostBody: AddCommentOnPostBody,
    @Param() findUniquePostParam: FindUniquePostParam,
    @AuthUser() user: User
  ) {
    return this.postService.addComment({
      postId: +findUniquePostParam.id,
      creatorId: user.id,
      message: addCommentOnPostBody.message,
    });
  }
}
