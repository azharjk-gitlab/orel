import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Request } from 'express';

import { PostService } from '@orel/api/data-access/post';

@Injectable()
export class PostCreatorGuard implements CanActivate {
  constructor(private postService: PostService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest() as Request;
    // FIXME: This guards run before the controller so no param validation.
    const id = req.params['id'];
    if (isNaN(+id)) return false;

    const post = await this.postService.findUniqueOrThrowById(+id);
    return post.creatorId === +id;
  }
}
