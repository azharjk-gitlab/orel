import { IsNotEmpty, IsString } from 'class-validator';

export class AddCommentOnPostBody {
  @IsNotEmpty()
  @IsString()
    message: string;
}
