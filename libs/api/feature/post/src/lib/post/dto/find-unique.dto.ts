import { IsNumberString } from 'class-validator';

export class FindUniquePostParam {
  @IsNumberString()
    id: number;
}
