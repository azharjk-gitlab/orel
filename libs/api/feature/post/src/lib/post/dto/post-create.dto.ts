import { IsNotEmpty } from 'class-validator';

import { Post } from '@orel/api/data-access/post';

export class PostCreateDto implements Post {
  @IsNotEmpty()
    content: string;
}
