import { IsNotEmpty } from 'class-validator';

export class PostUpdateDto {
  @IsNotEmpty() content: string;
}
