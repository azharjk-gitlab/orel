import { Module } from '@nestjs/common';

import { BcryptService } from './bcrypt/bcrypt.service';

@Module({
  controllers: [],
  providers: [BcryptService],
  exports: [BcryptService],
})
export class ApiUtilBcryptModule {}
