import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

@Injectable()
export class BcryptService {
  hash(password: string, salt: string) {
    return bcrypt.hash(password, salt);
  }

  compare(plainPassword: string, hashedPassword: string) {
    return bcrypt.compare(plainPassword, hashedPassword);
  }

  genSalt() {
    return bcrypt.genSalt();
  }
}
