import { Module } from '@nestjs/common';

import { ApiDataAccessCommentModule } from '@orel/api/data-access/comment';
import { ApiDataAccessPrismaModule } from '@orel/api/data-access/prisma';
import { PostService } from './post/post.service';

@Module({
  controllers: [],
  providers: [PostService],
  exports: [PostService],
  imports: [ApiDataAccessPrismaModule, ApiDataAccessCommentModule],
})
export class ApiDataAccessPostModule {}
