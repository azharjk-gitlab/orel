export interface Post {
  content: string;
  creatorId?: number;
}

export interface AddCommentOnPost {
  postId: number;
  creatorId: number;
  message: string;
}
