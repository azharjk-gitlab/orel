import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import {
  ApiDataAccessCommentModule,
  CommentService
} from '@orel/api/data-access/comment';
import {
  ApiDataAccessPrismaModule,
  PrismaService
} from '@orel/api/data-access/prisma';
import { PostService } from './post.service';
import { Post, AddCommentOnPost } from './post';

interface PostFindUniqueParam {
  include: {
    creator: {
      select: {
        email: boolean;
      };
    };
  };
  where: {
    id: number;
  };
}

const POST = {
  id: Math.floor(Math.random() * 10),
  content: 'somecontent',
  createdAt: new Date(),
  updatedAt: new Date(),
  creatorId: Math.floor(Math.random() * 10),
  creator: {
    email: 'king@mail.com',
  },
};

describe('PostService', () => {
  let postService: PostService;
  const prismaServiceMock = {
    post: {
      findMany: jest.fn(() => Promise.resolve([])),
      findUnique: jest.fn((args: PostFindUniqueParam) =>
        Promise.resolve({
          ...POST,
          id: args.where.id,
        })
      ),
      create: jest.fn((args: { data: Post }) =>
        Promise.resolve({
          ...POST,
          content: args.data.content,
          creatorId: args.data.creatorId,
        })
      ),
    },
  };
  const commentServiceMock = {
    findManyCommentOnPost: jest.fn(() => Promise.resolve([])),
    create: jest.fn((dto: AddCommentOnPost) =>
      Promise.resolve({
        postId: dto.postId,
        creatorId: dto.creatorId,
        message: dto.message,
      })
    ),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PostService],
      imports: [ApiDataAccessPrismaModule, ApiDataAccessCommentModule],
    })
      .overrideProvider(PrismaService)
      .useValue(prismaServiceMock)
      .overrideProvider(CommentService)
      .useValue(commentServiceMock)
      .compile();

    postService = module.get<PostService>(PostService);
  });

  it('should be defined', () => {
    expect(postService).toBeDefined();
  });

  describe('findMany', () => {
    it('should return array of posts', async () => {
      expect(await postService.findMany()).toBeInstanceOf(Array);

      expect(prismaServiceMock.post.findMany).toHaveBeenCalledWith({
        include: {
          creator: {
            select: {
              email: true,
            },
          },
        },
      });
    });
  });

  describe('findUniqueOrThrowById', () => {
    it('should throw not found exception if id is not exists', async () => {
      const id = 55;

      prismaServiceMock.post.findUnique.mockResolvedValueOnce(null);

      try {
        await postService.findUniqueOrThrowById(id);
      } catch (err) {
        expect(err).toBeInstanceOf(NotFoundException);
      }

      expect(prismaServiceMock.post.findUnique).toHaveBeenCalledWith({
        include: {
          creator: {
            select: {
              email: true,
            },
          },
        },
        where: {
          id,
        },
      });
    });

    it('should return post object with given id', async () => {
      const id = 43;

      expect(await postService.findUniqueOrThrowById(id)).toEqual({
        id: 43,
        content: expect.any(String),
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
        creatorId: expect.any(Number),
        creator: {
          email: expect.any(String),
        },
      });

      expect(prismaServiceMock.post.findUnique).toHaveBeenCalledWith({
        include: {
          creator: {
            select: {
              email: true,
            },
          },
        },
        where: {
          id,
        },
      });
    });
  });

  describe('create', () => {
    it('should return post object', async () => {
      const dto = {
        content: 'yow brow',
        creatorId: 55,
      };

      expect(await postService.create(dto)).toEqual({
        id: expect.any(Number),
        content: 'yow brow',
        createdAt: expect.any(Date),
        updatedAt: expect.any(Date),
        creatorId: 55,
        creator: {
          email: expect.any(String),
        },
      });

      expect(prismaServiceMock.post.create).toHaveBeenCalledWith({
        data: dto,
      });
    });
  });

  describe('findManyComment', () => {
    it('should return array of comments', async () => {
      const id = 55;

      expect(await postService.findManyComment(id)).toBeInstanceOf(Array);

      expect(commentServiceMock.findManyCommentOnPost).toHaveBeenCalledWith(id);
    });
  });

  describe('addComment', () => {
    it('should return comment object', async () => {
      const dto = {
        postId: 55,
        creatorId: 67,
        message: 'horray cool',
      };

      expect(await postService.addComment(dto)).toEqual({
        postId: 55,
        creatorId: 67,
        message: 'horray cool',
      });

      expect(commentServiceMock.create).toHaveBeenCalledWith(dto);
    });
  });
});
