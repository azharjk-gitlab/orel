import { Injectable, NotFoundException } from '@nestjs/common';

import { CommentService } from '@orel/api/data-access/comment';
import { PrismaService } from '@orel/api/data-access/prisma';
import { AddCommentOnPost, Post } from './post';

@Injectable()
export class PostService {
  constructor(private prisma: PrismaService, private commentService: CommentService) {}

  async findMany() {
    return await this.prisma.post.findMany({
      include: {
        creator: {
          select: {
            email: true,
          },
        },
      },
    });
  }

  async findUniqueOrThrowById(id: number) {
    const post = await this.prisma.post.findUnique({
      include: {
        creator: {
          select: {
            email: true,
          },
        },
      },
      where: {
        id,
      },
    });

    if (!post) throw new NotFoundException();

    return post;
  }

  async update(id: number, newPost: Post) {
    return await this.prisma.post.update({
      where: {
        id,
      },
      data: {
        content: newPost.content,
        updatedAt: new Date(),
      },
    });
  }

  async delete(id: number) {
    return await this.prisma.post.delete({
      where: {
        id,
      },
    });
  }

  async create(post: Post) {
    return await this.prisma.post.create({
      data: {
        content: post.content,
        creatorId: post.creatorId,
      },
    });
  }

  async findManyComment(id: number) {
    return await this.commentService.findManyCommentOnPost(id);
  }

  async addComment(data: AddCommentOnPost) {
    return await this.commentService.create(data);
  }
}
