import { Module } from '@nestjs/common';

import { ApiDataAccessPrismaModule } from '@orel/api/data-access/prisma';
import { CommentService } from './comment/comment.service';

@Module({
  controllers: [],
  providers: [CommentService],
  exports: [CommentService],
  imports: [ApiDataAccessPrismaModule],
})
export class ApiDataAccessCommentModule {}
