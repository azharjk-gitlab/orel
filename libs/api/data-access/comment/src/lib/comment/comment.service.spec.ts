import { Test, TestingModule } from '@nestjs/testing';

import { PrismaService } from '@orel/api/data-access/prisma';
import { CommentService } from './comment.service';

interface CommentFindManyArgs {
  include: {
    creator: {
      select: {
        email: boolean;
      };
    };
  };
  where: {
    postId: number;
  };
}

interface CommentCreateArgs {
  data: {
    message: string;
    creatorId: number;
    postId: number;
  };
}

const COMMENT = {
  id: Math.floor(Math.random() * 10),
  message: 'some',
  createdAt: new Date(),
  creatorId: Math.floor(Math.random() * 10),
  postId: Math.floor(Math.random() * 10),
  creator: {
    email: 'bob@mail.com',
  },
};

describe('CommentService', () => {
  let commentService: CommentService;
  const prismaServiceMock = {
    comment: {
      findMany: jest.fn((args: CommentFindManyArgs) => [
        {
          ...COMMENT,
          postId: args.where.postId,
        },
      ]),
      create: jest.fn((args: CommentCreateArgs) => ({
        ...COMMENT,
        message: args.data.message,
        postId: args.data.postId,
        creatorId: args.data.creatorId,
      })),
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CommentService, PrismaService],
    })
      .overrideProvider(PrismaService)
      .useValue(prismaServiceMock)
      .compile();

    commentService = module.get<CommentService>(CommentService);
  });

  it('should be defined', () => {
    expect(commentService).toBeDefined();
  });

  describe('findManyCommentOnPost', () => {
    it('should return array of comments', () => {
      const postId = 55;

      expect(commentService.findManyCommentOnPost(postId)).toBeInstanceOf(
        Array
      );

      expect(prismaServiceMock.comment.findMany).toHaveBeenCalledWith({
        include: {
          creator: {
            select: {
              email: true,
            },
          },
        },
        where: {
          postId,
        },
      });
    });
  });

  describe('create', () => {
    it('should return comment object', () => {
      const dto = {
        message: 'hello world',
        postId: 83,
        creatorId: 45,
      };

      expect(commentService.create(dto)).toEqual({
        id: expect.any(Number),
        message: 'hello world',
        createdAt: expect.any(Date),
        creatorId: 45,
        postId: 83,
        creator: {
          email: expect.any(String),
        },
      });

      expect(prismaServiceMock.comment.create).toHaveBeenCalledWith({
        data: {
          message: dto.message,
          creatorId: dto.creatorId,
          postId: dto.postId,
        },
      });
    });
  });
});
