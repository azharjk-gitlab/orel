import { Injectable } from '@nestjs/common';

import { PrismaService } from '@orel/api/data-access/prisma';

// FIXME: Due to circular dependencies
export interface AddCommentOnPost {
  postId: number;
  creatorId: number;
  message: string;
}

@Injectable()
export class CommentService {
  constructor(private prisma: PrismaService) {}

  findManyCommentOnPost(id: number) {
    return this.prisma.comment.findMany({
      include: {
        creator: {
          select: {
            email: true,
          },
        },
      },
      where: {
        postId: id,
      },
    });
  }

  create(data: AddCommentOnPost) {
    return this.prisma.comment.create({
      data: {
        message: data.message,
        creatorId: data.creatorId,
        postId: data.postId,
      },
    });
  }
}
