import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';

import { ApiDataAccessUserModule } from '@orel/api/data-access/user';
import { ApiUtilBcryptModule } from '@orel/api/util/bcrypt';
import { AuthService } from './auth/auth.service';
import { JwtStrategy } from './auth/strategy/jwt.strategy';

@Module({
  controllers: [],
  providers: [AuthService, JwtStrategy],
  exports: [AuthService],
  imports: [
    ApiDataAccessUserModule,
    ApiUtilBcryptModule,
    JwtModule.register({
      secret: process.env['JWT_SECRET'],
      signOptions: {
        expiresIn: '60s',
      },
    }),
  ],
})
export class ApiDataAccessAuthModule {}
