import { HttpException, HttpStatus } from '@nestjs/common';

export class EmailIsExistsException extends HttpException {
  constructor() {
    super({
      status: HttpStatus.OK,
      type: 'EMAIL_IS_EXISTS',
      message: 'Email is exists',
    }, HttpStatus.OK);
  }
}
