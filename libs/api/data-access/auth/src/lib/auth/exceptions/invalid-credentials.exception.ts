import { HttpException, HttpStatus } from '@nestjs/common';

export class InvalidCredentialsException extends HttpException {
  constructor() {
    super({
      status: HttpStatus.OK,
      type: 'INVALID_CREDENTIALS',
      message: 'Invalid credentials',
    }, HttpStatus.OK);
  }
}
