import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from '@prisma/client';

import { BcryptService } from '@orel/api/util/bcrypt';
import { UserService } from '@orel/api/data-access/user';
import { SignInDto } from './dto/signin.dto';
import { SignUpDto } from './dto/signup.dto';
import { EmailIsExistsException } from './exceptions/email-is-exists.exception';
import { InvalidCredentialsException } from './exceptions/invalid-credentials.exception';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
    private bcryptService: BcryptService
  ) {}

  signJwtToken(user: User) {
    const payload = {
      email: user.email,
      sub: user.id,
    };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async signIn(signInDto: SignInDto) {
    const user = await this.userService.findUniqueByEmail(signInDto.email);

    if (!user) throw new InvalidCredentialsException();

    const isPasswordMatch = await this.bcryptService.compare(
      signInDto.password,
      user.password
    );
    if (!isPasswordMatch) throw new InvalidCredentialsException();

    return this.signJwtToken(user);
  }

  async signUp(signUpDto: SignUpDto) {
    const exists = await this.userService.isEmailExists(signUpDto.email);
    if (exists) throw new EmailIsExistsException();

    // Now using the default salt which is 10
    const salt = await this.bcryptService.genSalt();
    const hashedPassword = await this.bcryptService.hash(
      signUpDto.password,
      salt
    );

    const user = await this.userService.create({
      email: signUpDto.email,
      password: hashedPassword,
    });

    return this.signJwtToken(user);
  }
}
