import { Module } from '@nestjs/common';

import { ApiDataAccessPrismaModule } from '@orel/api/data-access/prisma';
import { GlobalChatService } from './global-chat/global-chat.service';

@Module({
  controllers: [],
  providers: [GlobalChatService],
  exports: [GlobalChatService],
  imports: [ApiDataAccessPrismaModule],
})
export class ApiDataAccessGlobalChatModule {}
