import { Injectable } from '@nestjs/common';

import { PrismaService } from '@orel/api/data-access/prisma';
import { MessageInput } from './global-chat';

@Injectable()
export class GlobalChatService {
  constructor(private prismaService: PrismaService) {}

  async getMessages(userId: number) {
    const messages = await this.prismaService.globalChatMessage.findMany({
      include: {
        sender: {
          select: {
            email: true,
          },
        },
      },
    });

    const messagesMapped = messages.map((message) => {
      if (message.senderId === userId) return { ...message, isSender: true };
      return { ...message, isSender: false };
    });

    return messagesMapped;
  }

  addMessage(input: MessageInput) {
    return this.prismaService.globalChatMessage.create({
      data: {
        message: input.message,
        senderId: input.senderId,
      },
      include: {
        sender: {
          select: {
            email: true,
          },
        },
      },
    });
  }
}
