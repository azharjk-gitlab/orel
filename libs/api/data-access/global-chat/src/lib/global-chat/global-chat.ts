export interface MessageInput {
  message: string;
  senderId: number;
}
