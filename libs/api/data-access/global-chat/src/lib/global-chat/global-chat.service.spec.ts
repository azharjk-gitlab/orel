import { Test, TestingModule } from '@nestjs/testing';
import { GlobalChatService } from './global-chat.service';

describe('GlobalChatService', () => {
  let service: GlobalChatService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GlobalChatService],
    }).compile();

    service = module.get<GlobalChatService>(GlobalChatService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
