import { Test, TestingModule } from '@nestjs/testing';

import { PrismaService } from '@orel/api/data-access/prisma';
import { UserService } from './user.service';

const USER = {
  id: Math.floor(Math.random() * 10),
  email: 'somerandomemail@mail.com',
  password: 'password',
  createdAt: new Date(),
};

describe('UserService', () => {
  let userService: UserService;
  const prismaServiceMock = {
    user: {
      findUnique: jest.fn(
        (args: { where?: { email?: string; id?: number } }) => ({
          ...USER,
          id: args.where?.id ?? Math.floor(Math.random() * 10),
          email: args.where?.email ?? 'somerandomemail@mail.com',
        })
      ),
      findFirst: jest.fn((args: { where: { email: string } }) =>
        Promise.resolve({
          ...USER,
          email: args.where?.email ?? 'somerandomemail@mail.com',
        })
      ),
      create: jest.fn(
        (args: { data: { email: string; password: string } }) => ({
          ...USER,
          email: args.data.email,
          password: args.data.password,
        })
      ),
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserService, PrismaService],
    })
      .overrideProvider(PrismaService)
      .useValue(prismaServiceMock)
      .compile();

    userService = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(userService).toBeDefined();
  });

  describe('findUniqueById', () => {
    it('should return an user object with given id', () => {
      const id = 55;

      expect(userService.findUniqueById(id)).toEqual({
        id: 55,
        email: expect.any(String),
        password: expect.any(String),
        createdAt: expect.any(Date),
      });

      expect(prismaServiceMock.user.findUnique).toHaveBeenCalledWith({
        where: {
          id,
        },
      });
    });

    it('should return null if the id not exists', () => {
      const id = 404;

      prismaServiceMock.user.findUnique.mockReturnValueOnce(null);
      expect(userService.findUniqueById(id)).toBeNull();

      expect(prismaServiceMock.user.findUnique).toHaveBeenCalledWith({
        where: {
          id,
        },
      });
    });
  });

  describe('findUniqueByEmail', () => {
    it('should return an user object with given email', () => {
      const email = 'john@mail.com';

      expect(userService.findUniqueByEmail(email)).toEqual({
        id: expect.any(Number),
        email: 'john@mail.com',
        password: expect.any(String),
        createdAt: expect.any(Date),
      });

      expect(prismaServiceMock.user.findUnique).toHaveBeenCalledWith({
        where: {
          email,
        },
      });
    });

    it('should return null if the email is not exists', () => {
      const email = 'emailisnotexits@mail.com';

      prismaServiceMock.user.findUnique.mockReturnValueOnce(null);
      expect(userService.findUniqueByEmail(email)).toBeNull();

      expect(prismaServiceMock.user.findUnique).toHaveBeenCalledWith({
        where: {
          email,
        },
      });
    });
  });

  describe('isEmailExists', () => {
    it('should return true if the email is exists', async () => {
      const email = 'john@mail.com';

      expect(await userService.isEmailExists(email)).toBeTruthy();

      expect(prismaServiceMock.user.findFirst).toHaveBeenCalledWith({
        where: {
          email,
        },
      });
    });

    it('should return false if the email not exists', async () => {
      const email = '404@mail.com';

      prismaServiceMock.user.findFirst.mockResolvedValueOnce(null);
      expect(await userService.isEmailExists(email)).toBeFalsy();

      expect(prismaServiceMock.user.findFirst).toHaveBeenCalledWith({
        where: {
          email,
        },
      });
    });
  });

  describe('create', () => {
    it('should return user object', () => {
      const dto = {
        email: 'mike@mail.com',
        password: 'pwd',
      };

      expect(userService.create(dto)).toEqual({
        id: expect.any(Number),
        email: 'mike@mail.com',
        password: 'pwd',
        createdAt: expect.any(Date),
      });

      expect(prismaServiceMock.user.create).toHaveBeenCalledWith({
        data: {
          email: dto.email,
          password: dto.password,
        },
      });
    });
  });
});
