import { Injectable } from '@nestjs/common';

import { PrismaService } from '@orel/api/data-access/prisma';
import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

// FIXME: Temporary
export class SignUpDto {
  @IsEmail() email: string;

  @IsString() @IsNotEmpty() @MinLength(8) password: string;
}

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) {}

  async findMany(authUserId: number) {
    const users = await this.prisma.user.findMany({
      include: {
        followers: true,
        following: true,
      },
    });

    return await Promise.all(
      users.map(async (user) => {
        const { password, ...rest } = user;

        const isAlreadyFollowedObj =
          await this.prisma.followingFollowers.findFirst({
            where: {
              AND: [{ followerId: authUserId }, { followingId: user.id }],
            },
          });

        let isAlreadyFollowed = false;
        if (isAlreadyFollowedObj) isAlreadyFollowed = true;

        return {
          ...rest,
          isAlreadyFollowed,
        };
      })
    );
  }

  findUniqueByEmail(email: string) {
    return this.prisma.user.findUnique({
      where: {
        email,
      },
    });
  }

  findUniqueById(id: number) {
    return this.prisma.user.findUnique({
      where: {
        id,
      },
    });
  }

  async isEmailExists(email: string) {
    const exists = await this.prisma.user.findFirst({
      where: {
        email,
      },
    });

    return exists ? true : false;
  }

  create(signUpDto: SignUpDto) {
    return this.prisma.user.create({
      data: {
        email: signUpDto.email,
        password: signUpDto.password,
      },
    });
  }
}
