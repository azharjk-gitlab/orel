import { Module } from '@nestjs/common';

import { ApiDataAccessPrismaModule } from '@orel/api/data-access/prisma';
import { UserService } from './user/user.service';

@Module({
  controllers: [],
  providers: [UserService],
  exports: [UserService],
  imports: [ApiDataAccessPrismaModule],
})
export class ApiDataAccessUserModule {}
