# shared-ws

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build shared-ws` to build the library.

## Running unit tests

Run `nx test shared-ws` to execute the unit tests via [Jest](https://jestjs.io).
