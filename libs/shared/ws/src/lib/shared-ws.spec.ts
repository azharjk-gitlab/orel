import { sharedWs } from './shared-ws';

describe('sharedWs', () => {
  it('should work', () => {
    expect(sharedWs()).toEqual('shared-ws');
  });
});
