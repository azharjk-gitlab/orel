import { Component } from '@angular/core';

@Component({
  selector: 'orel-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'web';
}
