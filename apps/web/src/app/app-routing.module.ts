import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageNotFoundComponent } from '@orel/web/feature/page-not-found';
import { SigninComponent } from '@orel/web/feature/signin';
import { SignupComponent } from '@orel/web/feature/signup';
import { MeComponent } from '@orel/web/feature/me';
import { PostComponent } from '@orel/web/feature/post';
import { AuthGuard, WebDataAccessAuthModule } from '@orel/web/data-access/auth';
import { PostDetailComponent } from '@orel/web/feature/post-detail';
import { GlobalChatComponent } from '@orel/web/feature/global-chat';
import { UserListComponent } from '@orel/web/feature/user-list';

const routes: Routes = [
  { path: '', component: PostComponent, canActivate: [AuthGuard] },
  {
    path: 'posts/:id',
    component: PostDetailComponent,
    canActivate: [AuthGuard],
  },
  { path: 'me', component: MeComponent, canActivate: [AuthGuard] },
  { path: 'users', component: UserListComponent, canActivate: [AuthGuard] },
  {
    path: 'global-chat',
    component: GlobalChatComponent,
    canActivate: [AuthGuard],
  },
  { path: 'signin', component: SigninComponent },
  { path: 'signup', component: SignupComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), WebDataAccessAuthModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}
