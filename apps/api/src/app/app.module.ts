import { Module } from '@nestjs/common';

import { ApiFeatureAuthModule } from '@orel/api/feature/auth';
import { ApiFeatureGlobalChatModule } from '@orel/api/feature/global-chat';
import { ApiFeatureMeModule } from '@orel/api/feature/me';
import { ApiFeaturePostModule } from '@orel/api/feature/post';
import { ApiFeatureUserModule } from '@orel/api/feature/user';

@Module({
  imports: [
    ApiFeatureAuthModule,
    ApiFeatureMeModule,
    ApiFeaturePostModule,
    ApiFeatureGlobalChatModule,
    ApiFeatureUserModule,
  ],
})
export class AppModule {}
